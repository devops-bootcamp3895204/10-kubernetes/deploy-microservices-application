# Deploy Microservices Application

Demo Project:
Deploy Microservices application in Kubernetes with
Production & Security Best Practices
Technologies used:
Kubernetes, Redis, Linux, Linode LKE

Project description:
Create K8s manifests for Deployments and Services
for all microservices of an online shop application.
Deploy microservices to Linode’s managed Kubernetes
cluster.

Demo executed:
❏ Created YAML file with 11 Deployment and corresponding Service
manifests
❏ Note: All Services’ Components are Internal Services, except the Frontend
Service, which needs to be accessed from browser
❏ Created a K8s cluster with 3 Worker Nodes on Linode (or any other cloud
platform)
❏ Connected to the cluster
❏ Created a Namespace and deployed all the micro services into it
❏ Accessed Online Shop with Browser
